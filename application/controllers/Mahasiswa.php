<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Mahasiswa extends CI_Controller {
 

    function __construct(){
		parent::__construct();
        $this->load->model("Mahasiswa_model");
        $this->load->model("M_regis");
		if($this->session->userdata('logged') !=TRUE){
            $url=base_url('login');
            redirect($url);
		};
	}


	public function index()
	{
        $data["title"] = "List Data Mahasiswa";
        $data["data_mahasiswa"] = $this->Mahasiswa_model->getAll();
        $data['dosen'] = $this->Mahasiswa_model->dosen()->result();
        $foto['mahasiswa'] = $this->M_regis->mahasiswa();
		$this->load->view('tpls/sidebar', $foto);
		$this->load->view('vw_mahasiswa', $data);
		$this->load->view('tpls/footer');

	}

    public function pdf($id)
    {
        $this->load->model('mahasiswa_model');
		$data['mahasiswa'] = $this->Mahasiswa_model->getisi($id);
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-data-siswa.pdf";
		$this->pdf->load_view('laporan_siswa', $data);
    }

    public function export()
     {
          $semua_pengguna = $this->Mahasiswa_model->getData();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Nama')
                      ->setCellValue('C1', 'Email')
                      ->setCellValue('D1', 'NIM')
                      ->setCellValue('E1', 'Prodi')
                      ->setCellValue('F1', 'Proposal');

          $kolom = 2;
          $nomor = 1;
          foreach($semua_pengguna as $pengguna) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $nomor)
                           ->setCellValue('B' . $kolom, $pengguna->nama)
                           ->setCellValue('C' . $kolom, $pengguna->email)
                           ->setCellValue('D' . $kolom, $pengguna->nim)
                           ->setCellValue('E' . $kolom, $pengguna->nama_prodi)
                           ->setCellValue('F' . $kolom, $pengguna->proposal);

               $kolom++;
               $nomor++;

          }

          $writer = new Xlsx($spreadsheet);

          header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="Mahasiswa.xlsx"');
	  header('Cache-Control: max-age=0');

	  $writer->save('php://output');
     }



    

    public function add()
    {
        $Mahasiswa = $this->Mahasiswa_model; //objek model
        $this->form_validation->set_rules('proposal', 'proposal', 'required');
        $this->form_validation->set_rules('dosen', 'dosen', 'required');
        
        $this->form_validation->set_message('required', '{field} harus diisi!');
        $this->form_validation->set_message('is_unique', '{field} Sudah digunakan!');
        $this->form_validation->set_message('matches', '{field} harus sama dengan {param}!');

        if ($this->form_validation->run() === FALSE) {
            foreach ($this->input->post() as $key => $value) 
            {
                $res[$key] = form_error($key, '<div class="text-danger">', '</div>');
            }
            echo json_encode(['success' => false, 'message' => $res]);
        }else{
           /*  $id = $this->Mahasiswa_model->mahasiswa() */
            $Mahasiswa->save();//menerapkan rules validasi pada mahasiswa_model
            echo json_encode(['success' => true, 'message' => 'mahasiswa berasil ditambah']);
        }
       

       
    }

    public function edit($id=null)
    {
        if (!isset($id)) redirect('mahasiswa');

        $Mahasiswa = $this->Mahasiswa_model;
        $validation = $this->form_validation;
        $validation->set_rules($Mahasiswa->rules());
        $Mahasiswa->update($id);
        echo json_encode(['success' => true, 'message' => 'mahasiswa berasil diupdate']);
        
    }
    public function delete($id)
    {
        $deleted = $this->Mahasiswa_model->delete($id);

        if ($deleted){
            echo json_encode(['success' => true, 'message' => 'mahasiswa berasil didelete']);
        }
    }

    
    
}