<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Admin extends CI_Controller {

    function __construct(){
		parent::__construct();
        $this->load->model("Admin_model");
		if($this->session->userdata('logged') !=TRUE){
            $url=base_url('login');
            redirect($url);
		};
	}


	public function index()
	{
        $data["title"] = "List Data Mahasiswa";
        $data["data_mahasiswa"] = $this->Admin_model->getAll();
		$this->load->view('tpls/sidebar');
		$this->load->view('vw_admin', $data);
		$this->load->view('tpls/footer');

	}

	public function dosen(){
		$data["data_mahasiswa"] = $this->Admin_model->getAll();
		$this->load->view('tpls/sidebar');
		$this->load->view('vw_admin_dosen', $data);
		$this->load->view('tpls/footer');
	}

	public function get_admin()
   {


     /*  $query = $this->Admin_model->getAll(); */
	  $query = $this->Admin_model->admin();


      $data = [];


      foreach($query as $p => $r) {
    

           $data[] = array(
                $p+1,
                $r->nama,
                $r->nama_agama,
                $r->nama_prodi,
                $r->nim,
           );
      }


      $result = array(
                 "data" => $data
            );


      echo json_encode($result);
      /* exit(); */
   }
   public function get_admin_dosen()
   {


     /*  $query = $this->Admin_model->getAll(); */
	  $query = $this->Admin_model->dosen();


      $data = [];


      foreach($query as $p => $r) {
    

           $data[] = array(
                $p+1,
                $r->nama_d,
                $r->nip,
				'<div class="btn btn-secondary">edit</div>'
           );
      }


      $result = array(
                 "data" => $data
            );


      echo json_encode($result);
      /* exit(); */
   }

   public function tambah_dosen()
	{
		/* $this->form_validation->set_rules('nama_d', 'nama', 'trim|required|callback_checkAlphaOnly|min_length[6]'); */
		$this->form_validation->set_rules('nip', 'nomor induk pengajar', 'trim|required|is_natural|min_length[12]');
		$this->form_validation->set_rules('prodi', 'prodi', 'required');
		$this->form_validation->set_rules('email', 'prodi', 'required');
		$this->form_validation->set_rules('pendidikan_terakhir', 'prodi', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'required');
		$this->form_validation->set_rules('password', 'username', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        
        $this->form_validation->set_message('required', '{field} harus diisi!');
		$this->form_validation->set_message('checkAlphaOnly', '{field} harus di isi dengan huruf!');
		$this->form_validation->set_message('checkNomor', '{field} anda tidak sesuai, silahkan cek nomor telepone anda kembali');
		$this->form_validation->set_message('min_length', '{field} tidak mencukupi batas minimal!!');
		$this->form_validation->set_message('is_natural', '{field} harus berupa angka!!');
		$this->form_validation->set_message('matches', '{field} harus sama dengan password!!');
		$this->form_validation->set_message('valid_age', '{field} harus melebihi 16 tahun!!');

		if ($this->form_validation->run()===FALSE)
		
	   	{ 

			foreach ($this->input->post() as $key => $value) 
            {
                $res[$key] = form_error($key, '<div class="text-danger">', '</div>');
            }
            echo json_encode(['success' => false, 'message' => $res]);
		}
		else
		{
			
			
			/* $id_user = $this->M_regis->save(); */
			
			$user = array(
				'username' => $this->input->post('email'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
				'nama' => $this->input->post('nama'),
				/* 'id_group' => $this->input->post('id_group'), */
			);

			$this->db->insert('t_user',$user);
			$id_user = $this->db->insert_id();
			/* return $this->db->insert($this->table, $data); */

			$data = array(
				'id_user' => $id_user,
				'nama_d' => $this->input->post('nama'),
				'nip' => $this->input->post('nip'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                'prodi'=> $this->input->post('prodi')
			);

			$saved = $this->db->insert('dosen',$data);

			if ($saved) {
				echo json_encode(['success' => true, 'message' => 'dosen berasil ditambah']);
			}

		}
	}
}